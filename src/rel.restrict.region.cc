#include <string>
#include <list>
#include <iostream>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <unistd.h>
#include <string.h>
#include <endian.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <linux/uinput.h>
#include <glm/vec2.hpp>
#include "nlohmann/json.hpp"
#include <CLI/CLI.hpp>
#include <X11/Xlib.h>
#include <X11/extensions/Xrandr.h>
#include <X11/extensions/XInput.h>
#include <X11/extensions/XInput2.h>
#include <X11/extensions/XIproto.h>
#include <X11/extensions/XI2proto.h>
#include <X11/Xatom.h>

void send_event(int dev, int type, int code, int val)
{
    struct input_event ie;

    ie.type = type;
    ie.code = code;
    ie.value = val;
    /* timestamp values below are ignored */
    ie.time.tv_sec = 0;
    ie.time.tv_usec = 0;
    write(dev, &ie, sizeof(ie));
}

int uinput_setup_abs(int dev, const std::string &port) {
    struct uinput_abs_setup abs_setup;
    struct uinput_setup setup;
    volatile int r = 0;
    r |= ioctl(dev, UI_SET_EVBIT, EV_SYN);
    r |= ioctl(dev, UI_SET_EVBIT, EV_KEY);
    r |= ioctl(dev, UI_SET_KEYBIT, BTN_TOUCH);
    r |= ioctl(dev, UI_SET_KEYBIT, BTN_LEFT);
    r |= ioctl(dev, UI_SET_KEYBIT, BTN_RIGHT);
    r |= ioctl(dev, UI_SET_KEYBIT, BTN_MIDDLE);
    r |= ioctl(dev, UI_SET_EVBIT, EV_ABS);
    r |= ioctl(dev, UI_SET_EVBIT, EV_REL);
    r |= ioctl(dev, UI_SET_ABSBIT, ABS_X);
    r |= ioctl(dev, UI_SET_ABSBIT, ABS_Y);
    r |= ioctl(dev, UI_SET_RELBIT, REL_WHEEL);

    memset(&abs_setup, 0, sizeof(abs_setup));
    abs_setup.code = ABS_X;
    abs_setup.absinfo.value = 0;
    abs_setup.absinfo.minimum = 0;
    abs_setup.absinfo.maximum = INT16_MAX;
    abs_setup.absinfo.fuzz = 0;
    abs_setup.absinfo.flat = 0;
    abs_setup.absinfo.resolution = 400;
    r |= ioctl(dev, UI_ABS_SETUP, &abs_setup);

    memset(&abs_setup, 0, sizeof(abs_setup));
    abs_setup.code = ABS_Y;
    abs_setup.absinfo.value = 0;
    abs_setup.absinfo.minimum = 0;
    abs_setup.absinfo.maximum = INT16_MAX;
    abs_setup.absinfo.fuzz = 0;
    abs_setup.absinfo.flat = 0;
    abs_setup.absinfo.resolution = 400;
    r |= ioctl(dev, UI_ABS_SETUP, &abs_setup);

    memset(&setup, 0, sizeof(setup));
    snprintf(setup.name, UINPUT_MAX_NAME_SIZE, "restrict region %s", port.c_str());
    setup.id.bustype = BUS_VIRTUAL;
    setup.id.vendor  = 0x1;
    setup.id.product = 0x1;
    setup.id.version = 2;
    setup.ff_effects_max = 0;
    r |= ioctl(dev, UI_DEV_SETUP, &setup);
    r |= ioctl(dev, UI_DEV_CREATE);
    //    memset(&usetup, 0, sizeof(usetup));
    //    setup.id.bustype = BUS_USB;
    //    setup.id.vendor = 0x4711;
    //    setup.id.product = 0x0817;
    //    strcpy(usetup.name, "MT3M");
    return r;
}

int uinput_setup_rel(int dev, const std::string &port) {
    struct uinput_abs_setup abs_setup;
    struct uinput_setup setup;
    volatile int r = 0;
    r |= ioctl(dev, UI_SET_EVBIT, EV_SYN);
    r |= ioctl(dev, UI_SET_EVBIT, EV_KEY);
    r |= ioctl(dev, UI_SET_KEYBIT, BTN_LEFT);
    r |= ioctl(dev, UI_SET_KEYBIT, BTN_RIGHT);
    r |= ioctl(dev, UI_SET_KEYBIT, BTN_MIDDLE);
    r |= ioctl(dev, UI_SET_EVBIT, EV_REL);
    r |= ioctl(dev, UI_SET_RELBIT, REL_X);
    r |= ioctl(dev, UI_SET_RELBIT, REL_Y);
    r |= ioctl(dev, UI_SET_RELBIT, REL_WHEEL);
    memset(&setup, 0, sizeof(setup));
    snprintf(setup.name, UINPUT_MAX_NAME_SIZE, "rel restrict region %s", port.c_str());
    setup.id.bustype = BUS_VIRTUAL;
    setup.id.vendor  = 0x1;
    setup.id.product = 0x1;
    setup.id.version = 2;
    setup.ff_effects_max = 0;
    r |= ioctl(dev, UI_DEV_SETUP, &setup);
    r |= ioctl(dev, UI_DEV_CREATE);
    return r;
}

struct XOutput {
    std::string name;
    glm::ivec2 offset;
    glm::ivec2 size;
};

std::list<XOutput> XREnum() {
    std::list<XOutput> result;
    auto dpy = XOpenDisplay(NULL);
    auto root_wnd = RootWindow(dpy, 0);
    auto resources = XRRGetScreenResources(dpy, root_wnd);
    //    {
    //        std::cout << "+++++++++++++++++++++++++++++" << std::endl;
    //        for(int i = 0; i < resources->ncrtc; i++) {
    //            auto crtc_info = XRRGetCrtcInfo(dpy, resources, resources->crtcs[i]);
    //            printf("%d x %d @ %d,%d  / %d\n", crtc_info->width, crtc_info->height, crtc_info->x, crtc_info->y, crtc_info->noutput);
    //        }
    //        std::cout << "+++++++++++++++++++++++++++++" << std::endl;
    //    }
    for(int i = 0; i < resources->noutput; i++) {
        //        std::cout << "#############################" << std::endl;
        auto output_info = XRRGetOutputInfo(dpy, resources, resources->outputs[i]);
        //        std::cout << output_info->name  << " " << output_info->connection << std::endl;
        if(output_info->crtc > 0){
            auto crtc_info = XRRGetCrtcInfo(dpy, resources, output_info->crtc);
            //            std::cout << crtc_info->width << " " << crtc_info->height << " " << crtc_info->x << " " << crtc_info->y << " " << std::endl;
            result.push_back({output_info->name, {crtc_info->x, crtc_info->y}, {crtc_info->width, crtc_info->height}});
        }
        //        std::cout << "-----------------------------" << std::endl;
        for(int k = 0; k < output_info->ncrtc; k++) {
            auto crtc_info = XRRGetCrtcInfo(dpy, resources, output_info->crtcs[k]);
            //            std::cout << crtc_info->width << " " << crtc_info->height << " " << crtc_info->x << " " << crtc_info->y << " " << std::endl;
        }
    }
    XCloseDisplay(dpy);
    return result;
}

struct UInput {
    long unsigned int id;
    std::string name;
    std::string devnode;
};

std::list<UInput> XIEnum() {
    std::list<UInput> result;
    auto dpy = XOpenDisplay(NULL);
    int num_dev;
    auto prop = XInternAtom (dpy, "Device Node", False);
    auto list = XListInputDevices(dpy, &num_dev);
    for(int i = 0; i < num_dev; i ++) {
        //        printf("id=%i\tuse=%i\n", list[i].id, list[i].use);
        if(list[i].use == IsXExtensionPointer) {
            auto id = list[i].id;
            auto dev = XOpenDevice(dpy, id);
            int nprops;
            Atom realtype;
            unsigned long nitems, bytes_after;
            unsigned char *data;
            int realformat;
            std::string dev_name;
            auto properties = XListDeviceProperties(dpy, dev, &nprops);
            for(int k = 0; k < nprops; k++) {
                if(properties[k] == prop) {
                    XGetDeviceProperty(dpy, dev, properties[k], 0, 32, False, XA_STRING, &realtype, &realformat, &nitems, &bytes_after, &data);
                    char *str = (char*)data;
                    //                    printf("[%s]\n", str);
                    dev_name = std::string(str);
                    result.push_back({id, list[i].name, dev_name});
                    XFree(data);
                }
            }
            XCloseDevice(dpy, dev);
            XFree(properties);
        }
    }
    XFreeDeviceList(list);
    XCloseDisplay(dpy);
    return result;
}

void EnableDevice(int id, bool enable) {
    auto dpy = XOpenDisplay(NULL);
    auto prop_enable = XInternAtom (dpy, "Device Enabled", False);
    Atom properties;
    auto dev = XOpenDevice(dpy, id);
    int nprops;
    Atom realtype;
    unsigned long nitems, bytes_after;
    unsigned char *data;
    int realformat;
    XGetDeviceProperty(dpy, dev, prop_enable, 0, 32, False, XA_INTEGER, &realtype, &realformat, &nitems, &bytes_after, &data);
    XChangeDeviceProperty(dpy, dev, prop_enable, realtype, realformat, PropModeReplace, (const unsigned char*)&enable, 1);
    XCloseDevice(dpy, dev);
    XCloseDisplay(dpy);
}

template <typename T = int> struct ScreenRect {
    typedef glm::vec<2,T> vec;
    vec lu, rd, wh;
    void UpdateRD(){ rd = lu + wh; }
    void UpdateLU(){ lu = rd - wh; }
    void UpdateWH(){ wh = rd - lu; }
};

bool g_flag_term = false;

void sig_term(int signum) {
    g_flag_term = true;
}

int main(int argc, char **argv)
{
    CLI::App app("cursor.region.daemon");
    std::string dev_name;
    std::vector<std::string> output_dev_names;
    //    std::string output_dev_name;
    bool f_mode_abs{false};
    bool f_enum_all{false};
    bool f_whole{false};
    bool f_input_dev_id{false};
    bool f_daemonize{false};
    ScreenRect<> metrascreen = {{0, 0}, {0, 0}};
    ScreenRect<> outputscreen = {{INT32_MAX, INT32_MAX}, {INT32_MIN, INT32_MIN}};
    ScreenRect<> region;
    std::vector<int> region_rect{0, 0, 640, 480};
    app.add_option("--input,input", dev_name, "input device");
    app.add_option("-o,--output", output_dev_names, "output device");
    app.add_option("-r,--rect", region_rect, "set restrict rectangle region (L U R D)");
    app.add_flag("-i, --id", f_input_dev_id, "input device id");
    app.add_flag("-w,--whole", f_whole, "set region for whole set of outputs");
    app.add_flag("-m,--mode", f_mode_abs, "force set mode absolute for some transcendent Xorg versions");
    app.add_flag("-e,--enum", f_enum_all, "enum display outputs and input devices");
    app.add_flag("-d,--daemonize", f_daemonize, "daemonize process (SIGINT or SIGTERM to exit)");
    auto outputs = XREnum();
    auto inputs = XIEnum();

    CLI11_PARSE(app, argc, argv);

    int r;
    glm::vec2 point, pointCalib;

    if(f_enum_all) {
        std::cout << "enum device" << std::endl;
        std::cout << "video outputs:" << std::endl;
        for(auto out: outputs) {
            std::cout << out.name << ": " << out.offset.x << "," << out.offset.y << " " << out.size.x << "x" << out.size.y <<std::endl;
        }
        std::cout << "user inputs:" << std::endl;
        for(auto in: inputs) {
            std::cout << in.name << ": id = " << in.id << ", device node = " << in.devnode << std::endl;
        }
        return 0;
    }

//    daemonize();
    if(f_daemonize)daemon(0, 0);
    signal(SIGTERM, sig_term);
    signal(SIGINT, sig_term);

    //find dev node
    std::string dev_node;
    unsigned int id = -1;
    if(f_input_dev_id) {
        id = std::atoi(dev_name.c_str());
    }
    for(auto in: inputs) {
        if(f_input_dev_id) {
            if(id == in.id) {
                dev_node = in.devnode;
                break;
            }
        } else {
            if(dev_name == in.name) {
                id = in.id;
                dev_node = in.devnode;
                break;
            }
        }
    }
    EnableDevice(id, false);

    //find output resolution and maximum combined metascreen
    if(output_dev_names.empty()) {
        for(auto out: outputs) {
            output_dev_names.push_back(out.name);
        }
    }
    for(auto out: outputs) {
        ScreenRect<> rect;
        rect.lu = out.offset;
        rect.wh = out.size;
        rect.UpdateRD();
        if(metrascreen.lu.x > rect.lu.x) metrascreen.lu.x = rect.lu.x;
        if(metrascreen.lu.y > rect.lu.y) metrascreen.lu.y = rect.lu.y;
        if(metrascreen.rd.x < rect.rd.x) metrascreen.rd.x = rect.rd.x;
        if(metrascreen.rd.y < rect.rd.y) metrascreen.rd.y = rect.rd.y;
        for(auto outdev: output_dev_names) {
            if(out.name == outdev) {
                if(outputscreen.lu.x > rect.lu.x) outputscreen.lu.x = rect.lu.x;
                if(outputscreen.lu.y > rect.lu.y) outputscreen.lu.y = rect.lu.y;
                if(outputscreen.rd.x < rect.rd.x) outputscreen.rd.x = rect.rd.x;
                if(outputscreen.rd.y < rect.rd.y) outputscreen.rd.y = rect.rd.y;
            }
        }
    }
    outputscreen.UpdateWH();
    metrascreen.UpdateWH();

    const char *dev_node_c = dev_node.c_str();
    int dev_ui_abs = open("/dev/uinput", O_WRONLY | O_NONBLOCK);
    if(dev_ui_abs < 0) {
        printf("Failed to open uinput device\n");
        return -1;
    }
    r = uinput_setup_abs(dev_ui_abs, dev_name);
    printf("uinput_setup_abs = %d\n", r);

    if(r) return -1;
    // set mode absolute if required
    if(f_mode_abs) {
        usleep(100000);
        char str[1024];
        sprintf(str, "xinput set-mode \"rel restrict region %s\" ABSOLUTE", dev_name.c_str());
        system(str);
    }
    int dev_mouse = open(dev_node_c, 0);
    if(dev_mouse < 0) {
        printf("Failed to open mouse device %s\n", dev_node_c);
        return -1;
    }
    glm::ivec2 pos = {0, 0}, delta = {0, 0};
    if(f_whole) {
        region = {{0, 0}, outputscreen.wh, outputscreen.wh};
    } else {
        region = {{region_rect[0], region_rect[1]}, {region_rect[2], region_rect[3]}};
    }
    // loop
    while (1) {
        uint8_t raw[32];
        int r = read(dev_mouse, raw, 32);
        if(g_flag_term) {
            close(dev_mouse);
            close(dev_ui_abs);
            EnableDevice(id, true);
            exit(0);
        }
        if(r < 24) continue;
        struct input_event *ie = (struct input_event*)raw;
        if(ie->type == EV_REL) {
            delta = {0, 0};
            if(ie->code == REL_X) delta = {ie->value, 0};
            if(ie->code == REL_Y) delta = {0, ie->value};
            pos += delta;
        } else {
            if(ie->type == EV_KEY)
                send_event(dev_ui_abs, ie->type, ie->code, ie->value);
            send_event(dev_ui_abs, EV_SYN, SYN_REPORT, 0);
            continue;
        }
        if(pos.x < region.lu.x) pos.x = region.lu.x;
        if(pos.x > region.rd.x) pos.x = region.rd.x;
        if(pos.y < region.lu.y) pos.y = region.lu.y;
        if(pos.y > region.rd.y) pos.y = region.rd.y;
        if(ie->type == EV_REL && ie->code == REL_X) send_event(dev_ui_abs, EV_ABS, ABS_X, (pos.x + outputscreen.lu.x) * INT16_MAX / metrascreen.wh.x);
        if(ie->type == EV_REL && ie->code == REL_Y) send_event(dev_ui_abs, EV_ABS, ABS_Y, (pos.y + outputscreen.lu.y) * INT16_MAX / metrascreen.wh.y);
        if(ie->type == EV_REL && ie->code == REL_WHEEL) send_event(dev_ui_abs, EV_REL, REL_WHEEL, ie->value);
        send_event(dev_ui_abs, EV_SYN, SYN_REPORT, 0);
        continue;
    }
    return 0;
}

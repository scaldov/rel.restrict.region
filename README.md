## Linux uinput relative device region restriction daemon written in C++.

#### Utilizes:
+ UINPUT to create virtual positioning device with absolute coordinates and relative mouse wheel axis.
+ libXrandr to enumerate connected outputs and its sizes
+ libXi to disable and enable back the original mouse along with appropriate  input device enumeration.
### Usage examples.
+ Help
```
cursor.region.daemon
Usage: rel.restrict.region [OPTIONS] [input]

Positionals:
  input TEXT                  input device

Options:
  -h,--help                   Print this help message and exit
  --input TEXT                input device
  -o,--output TEXT ...        output device
  -r,--rect INT ...           set restrict rectangle region (L U R D)
  -i,--id                     input device id
  -w,--whole                  set region for whole set of outputs
  -m,--mode                   force set mode absolute for some transcendent Xorg versions
  -e,--enum                   enum display outputs and input devices
  -d,--daemonize              daemonize process (SIGINT or SIGTERM to exit)
```
+ Device enumeration:
```
./build/rel.restrict.region -e
enum device
video outputs:
DVI-D-0: 0,0 1600x1200
HDMI-0: 1600,0 1920x1080
user inputs:
SONiX USB DEVICE Keyboard: id = 9, device node = /dev/input/event5
UGTABLET 6 inch PenTablet Mouse: id = 12, device node = /dev/input/event0
Logitech Wireless Mouse: id = 10, device node = /dev/input/event3
```

+ Set region to one of the output
```
sudo rel.restrict.region "Logitech Wireless Mouse" -o "DVI-D-0" -w
```
In this example mouse region restricted to right output screen connected to DVI.

+ Set region to number of outputs
```
sudo rel.restrict.region "Logitech Wireless Mouse" -o "DVI-D-0" "HDMI-0" -w
```
In this example mouse region restricted to both screens.

+ Set basis region to number of outputs and set restriction rectangle inside it.
```
sudo rel.restrict.region "Logitech Wireless Mouse" -o "DVI-D-0" "HDMI-0" -r 100 100 2800 800
```

+ Set basis region to all connected outputs and set restriction rectangle inside it.
```
sudo rel.restrict.region "Logitech Wireless Mouse" -r 100 100 2800 800
```

#### xinput information
```
xinput
⎡ Virtual core pointer                    	id=2	[master pointer  (3)]
⎜   ↳ Virtual core XTEST pointer              	id=4	[slave  pointer  (2)]
⎜   ↳ SONiX USB DEVICE Keyboard               	id=9	[slave  pointer  (2)]
⎜   ↳ UGTABLET 6 inch PenTablet Mouse         	id=12	[slave  pointer  (2)]
⎜   ↳ restrict region Logitech Wireless Mouse 	id=17	[slave  pointer  (2)]
⎜   ↳ Logitech Wireless Mouse                 	id=10	[slave  pointer  (2)]
⎣ Virtual core keyboard                   	id=3	[master keyboard (2)]
    ↳ Virtual core XTEST keyboard             	id=5	[slave  keyboard (3)]
    ↳ Power Button                            	id=6	[slave  keyboard (3)]
    ↳ Power Button                            	id=7	[slave  keyboard (3)]
    ↳ SONiX USB DEVICE                        	id=8	[slave  keyboard (3)]
    ↳ HD USB Camera                           	id=11	[slave  keyboard (3)]
    ↳ UGTABLET 6 inch PenTablet Keyboard      	id=13	[slave  keyboard (3)]
    ↳ UGTABLET 6 inch PenTablet               	id=14	[slave  keyboard (3)]
    ↳ ACPI Virtual Keyboard Device            	id=15	[slave  keyboard (3)]
    ↳ SONiX USB DEVICE Keyboard               	id=16	[slave  keyboard (3)]
```
Here it could be seen that virtual ***`restrict region Logitech Wireless Mouse`*** attached.

Daemon can be terminated with `-TERM` or `-INT` signal. Before exit it re-enables original mouse device.

#! /usr/bin/env python
import os
import time

project = 'microtouch'
top = '.'
out = 'build'

f_sh = 0

def options(ctx):
    ctx.load('compiler_c')
    ctx.load('compiler_cxx')

def configure(ctx):
    ctx.env.VERSION = os.popen('git rev-parse --short HEAD').read().rstrip()
    ctx.env.TIMESTAMP = str(int(time.time()))
    ctx.env.DEFINES = ['VERSION=0x'+ctx.env.VERSION, 'COMPILE_TIMESTAMP='+ctx.env.TIMESTAMP, 'DEBUG=0', 'WAF=1', 'QT_NO_VERSION_TAGGING']
    print('→ configuring the project')
    ctx.find_program('strip', var='STRIP')
    ctx.find_program('gcc', var='CC')
    ctx.find_program('g++', var='CXX')
    ctx.load('compiler_c')
    ctx.load('compiler_cxx')
    ctx.env.CFLAGS = '-Os -fPIC'.split()
    ctx.env.CFLAGS += '-g -ggdb -Wno-unused-variable -Wno-format -Wno-unused-but-set-variable -Wall -fopenmp -static'.split()
    ctx.env.CXXFLAGS = [] + ctx.env.CFLAGS
    ctx.env.CFLAGS += '-std=gnu99'.split()
    ctx.env.CXXFLAGS += '-std=gnu++1z'.split()
    ctx.env.LDFLAGS = ' -fopenmp'.split()
    #ctx.load('qt5')

def clean(ctx):
    print('Cleaned.')

def build(ctx):
#    ctx.add_post_fun(post)
    libs = 'libusb-1.0'.split()
    lib_flags_c = []
    lib_flags_ld = []
    for lib in libs:
        lib_flags_c += os.popen('pkg-config --cflags ' + lib).read().rstrip().split()
        lib_flags_ld += os.popen('pkg-config --libs ' + lib + ' | sed -e "s/-l//" ').read().rstrip().split()
    source_objlib=['src/' + s for s in ''.split()]
    include_path = ctx.env.INCLUDES_QT5WIDGETS + 'build/src json-develop/include include'.split()
    source_crdaemon=['src/' + s for s in 'rel.restrict.region.cc'.split()]
    ctx.objects(target='objlib', source = source_objlib, includes = include_path, cxxflags = lib_flags_c)
    ctx.program(target='rel.restrict.region', source = source_crdaemon, includes = include_path, cxxflags = lib_flags_c, use=[], lib=['pthread', 'Xrandr', 'Xi', 'X11'] + lib_flags_ld)

from waflib import TaskGen

